# SearchEmail

This code presents a fast and simple solution for a Front-end Engineer Test. But, even though is a sort of "mock-up", the design aims to be maintainable and abstract. In that way, this code can not only serve as a mock-up but also as a template for other noncomplex web solutions.

The structure of the code is the most important thing to understand. Having an idea of where to do every part of the solution simplifies the solution itself. The structure is:

- public/
----- components/
---------- appbar.html
---------- bullet.html
---------- footer.html
---------- searchform.html
----- css/
---------- bullet.css
---------- input.css
---------- modal.css
---------- resultscard.css
---------- style.css
----- images/
---------- icn_person.svg
---------- icn_search.svg
---------- lock.svg
---------- Logo.svg
----- js/
---------- include.js
---------- results.js
---------- search.js
----- text/
---------- data.json
- views/
---------- results.ejs
---------- search.ejs
- .gitignore
- package.json
- app.js
- Procfile              

    *For more information, each file has a brief explanation at the beginning.

The folder /components contain HTML elements for components that felt too big to be in the main view or that are used in more than one view. Those are imported into the main view files later.

/css contains styles for the solution. I'm gonna be honest, it's a bit messy, especially the file style.css but every important configuration as a comment indicating the purpose. Here some files have different configurations for the phones (320px), tables (780px) and PCs (1440px).

/images folder as the images from the specs, in .svg because it doesn't lose quality.

The folder /js contains javascript code to simplify the coding and to get the information from the API. There's also some logic to get the strings necessary for the view. I did that because the Latin phrase was used a lot of times and it felt wrong to just copy and paste it that much. Also, if later we want to use different languages, the /text folder can have a .json file for each language.

The /views hold the *two* pages necessary for the solution. I call the first one search and the other one results. The logic behind the transition is that the first page accepts the input email but it's actually the second page that makes the request itself. The only thing saved on the session storage is the last email requested. I had to use a proxy for the request because of CORS. I had seen that sometimes this service (Heroku) fails to make the request. All weird cases that I saw are documented in my personal record and also raise error messages to the user (if that helps).

Express is used to create static assets and Heroku is used for deployment. For any questions, you can contact me at my email gergerger2009@gmail.com

page: https://nameless-wave-18117.herokuapp.com/