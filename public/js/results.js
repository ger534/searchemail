/*script for making request and filling results card*/

// Get the modal
var modal = document.getElementById("loadingModal");
console.log(modal)

// Get the button that opens the modal
var btn = document.getElementById("requestBtn");

modal.style.display = "block";

const queryString = window.location.search;
console.log(queryString);

const urlParams = new URLSearchParams(queryString);
//const email = urlParams.get("email");
const email = localStorage.getItem("email")
console.log(email);

var api = `https://cors-anywhere.herokuapp.com/https://ltv-data-api.herokuapp.com/api/v1/records.json?email=${email}`;

fetch(api)
  .then(
    (response) => response.text()
  )
  .then((data) => {
    console.log(data)
    if (data == undefined) {
      console.log("error data undefined")


    }
    data = JSON.parse(data);

    document.getElementById("address").innerHTML = data["address"];

    var phones = data["phone_numbers"].toString().split(',');
    let info_phones = ""
    for (var i = 0; i < phones.length; i++) {
      //expected (555) 555-5555
      let phone = phones[i].toString()
      phone = "("+phone[0]+phone[1]+phone[2]+") "+phone[3]+phone[4]+phone[5]+"-"+phone[6]+phone[7]+phone[8]
      info_phones += phone + '\n'
    }
    document.getElementById("phone").innerHTML = info_phones;


    document.getElementById("email").innerHTML = data["email"];

    
    console.log(data["relatives"])
    var relatives = data["relatives"];
    let info_relatives = ""
    for (var i = 0; i < relatives.length; i++) {
      console.log(relatives[i])
      info_relatives += relatives[i].toString() + '\n'
    }
    document.getElementById("relatives").innerHTML = info_relatives;
    
    //document.getElementById("relatives").innerHTML = data["relatives"];
    
    
    document.getElementById("name").innerHTML = data["first_name"];

    document.getElementById("last_name").innerHTML = data["last_name"];
    document.getElementById("description").innerHTML = data["description"];
    console.log(data)
    if (data.length == 0) {
      alert("request successful but response empty");
      document.getElementById("address").innerHTML = "empty";
      document.getElementById("phone").innerHTML = "empty";
      document.getElementById("email").innerHTML = "empty";
      document.getElementById("relatives").innerHTML = "empty";

      document.getElementById("name").innerHTML = "empty";
      document.getElementById("last_name").innerHTML = "empty";
      document.getElementById("description").innerHTML = "empty";

    }
    modal.style.display = "none";

  })
  .catch((error) => {

    console.log(error.message);
    modal.style.display = "none";
    alert("api not responding, try again please");
    document.location.href = "/"

  });

//load text from json
$.getJSON("text/data.json", function (data) {
  document.getElementById("title").innerHTML = data["results"].title;
  document.getElementById("subtitle_1").innerHTML = data["results"].subtitle_1;
  document.getElementById("subtitle_2").innerHTML = data["results"].subtitle_2;

});